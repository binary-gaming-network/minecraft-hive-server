package au.com.vipergaming.hive;

import com.google.inject.Inject;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.ServerInfo;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Plugin(id = "hive-server", name = "Hive", version = "1.0", description = "Dynamically fetch running hydra servers and add to server listing.", authors = { "Jan Jaso - A Slav from the village." })
class Hive
{
    private static class ClientHandler implements Runnable {
		private final Socket socket;
		private final ProxyServer server;
		private final Logger logger;

		private ServerInfo hydraClient;
		private int connDuration = 0;
        private static final int MINECRAFT_PORT = 25565;

		ClientHandler(Socket sock, ProxyServer ser, Logger log) {
			this.socket = sock;
			this.server = ser;
			this.logger = log;
		}

		@Override
		public void run() {
            logger.info("Hydra Client Connected: " + socket);
			try {
                Scanner in = new Scanner(socket.getInputStream());
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

                boolean serverAdded = false;

                while(true)
                {
                    if(!serverAdded)
                    {
                        InetSocketAddress ipAddress = new InetSocketAddress(socket.getInetAddress(), MINECRAFT_PORT);
                        String serverName = "";
                        if(in.hasNextLine())
                        {
                            serverName = in.nextLine();//hydra will send its server name once upon connecting //blocking once
                        }
                        hydraClient = new ServerInfo(serverName, ipAddress);

                        server.registerServer(hydraClient);
                        server.getConfiguration().getAttemptConnectionOrder().add(hydraClient.getName());
                        logger.info("Added new hydra client '" + hydraClient.getName()+"'");

                        serverAdded = true;//prevents adding twice, and stops blocking thread loop after running once
                    }

                    //send a ping out to client
                    out.print('.');
                    if (out.checkError())
                    {
                        logger.info("Could not send ping to Hydra Client (socket closed).");
                        //check if server is registered before attempting removal - pro style
                        if(server.getServer(hydraClient.getName()).isPresent())
                        {
                            server.unregisterServer(hydraClient);
                            server.getConfiguration().getAttemptConnectionOrder().remove(hydraClient.getName());
                        }
                        logger.info("Removed hydra client '" + hydraClient.getName() + "' | Total UpTime: " + connDuration + " seconds");
                        break;//exit while loop - finally will close connection
                    }
                    //Check once per second.
                    connDuration++;
                    try { Thread.sleep(1000); } catch (InterruptedException e) {logger.error("Error:" + e.getMessage());}
                }

			} catch (Exception e) {
                logger.error("Error:" + e.getMessage());
			} finally {
				try { socket.close(); } catch (IOException e) { logger.error("Error:" + e.getMessage()); }
                logger.info("Socket Closed: " + socket);
			}
		}
	}

    private static class HiveServer implements Runnable {
        private static final int HIVE_SERVER_PORT = 25599;
        private final Logger logger;
        private final ProxyServer server;

        HiveServer(ProxyServer ser, Logger log) {
            this.server = ser;
            this.logger = log;
        }

        @Override
        public void run() {
            //Start the listener for Hydra Clients to connect.
            try (ServerSocket listener = new ServerSocket(HIVE_SERVER_PORT)) {
                final int SERVER_CAPACITY = 100;
                logger.info(String.format("Hive Server (%s Client Max) is now running on %s", SERVER_CAPACITY, listener.getLocalSocketAddress()));
                ExecutorService pool = Executors.newFixedThreadPool(SERVER_CAPACITY);
                while (true) {
                    pool.execute(new ClientHandler(listener.accept(), server, logger));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Inject
    public Hive(ProxyServer server, Logger logger) {
        //init
        logger.info("Initializing Hive Server");

        //Remove dummy server if its there (it will be).
        if (server.getServer("Lobby").isPresent()) {
            ServerInfo dummyServer = server.getServer("Lobby").get().getServerInfo();
            server.unregisterServer(dummyServer);
            logger.info("Removed dummy server: " + dummyServer);
        }

        ExecutorService pool = Executors.newFixedThreadPool(1);//1 thread only for listener
        pool.execute(new HiveServer(server, logger));//Run listener in background so plugin doesn't stop game from starting
        logger.info("Hive Server Running in Background - Continue Main Program");
    }
}